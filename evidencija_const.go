package data

const sqlInsertEvidencija = `
INSERT INTO evidencija (
	id_zaposlenika, ime, prezime, tablica
) VALUES (
	?, ?, ?, ?
)
`

const sqlSelectAllEvidencija = `
SELECT
	*
FROM
	staff
`

const sqlSelectEvidencijaByID = `
SELECT 
	*
FROM 
	staff
WHERE
	id = $1
`

const sqlSelectEvidencijaByName = `
SELECT 
	*
FROM 
	staff
WHERE
	UPPER(ime) = UPPER(?)
`

const sqlSelectEvidencijaBySurname = `
SELECT 
	*
FROM 
	staff
WHERE
	UPPER(prezime) = UPPER(?)
`

const sqlSelectEvidencijaByTable = `
SELECT 
	*
FROM 
	staff
WHERE	
	UPPER(tablica) = UPPER(?)
`
