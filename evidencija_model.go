package data

import (
	"database/sql"
	"errors"

	"../db"
)

// Evidencija represents information about evidencija
type Evidencija struct {
	ZaposlenikID int    `json:"id_zaposlenika"`
	Name         string `json:"ime"`
	Surname      string `json:"prezime"`
	Plate        string `json:"tablica"`
}

// Evidencije represents list of all evidencije in database
type Evidencije struct {
	EvidencijaList []Evidencija `json:"evidencije"`
}

// func getEvidencijaByID(db *db.Database, id int) (*Evidencija, error) {
// 	return getEvidencija(db, sqlSelectEvidencijaByID, fmt.Sprint(id))
// }

// func getEvidencijaByName(db *db.Database, name string) (*Evidencije, error) {
// 	return getEvidencijeWithParam(db, sqlSelectEvidencijaByName, name)
// }

// func getEvidencijaBySurname(db *db.Database, name string) (*Evidencije, error) {
// 	return getEvidencijeWithParam(db, sqlSelectEvidencijaBySurname, name)
// }

func getEvidencijaByTable(db *db.Database, table string) (*Evidencija, error) {
	return getEvidencija(db, sqlSelectEvidencijaByTable, table)
}

func getAllEvidencije(db *db.Database) (*Evidencije, error) {
	return getEvidencije(db, sqlSelectAllEvidencija)
}

func getEvidencija(db *db.Database, query string, param string) (*Evidencija, error) {
	e := &Evidencija{}

	err := db.Conn.QueryRow(query, param).Scan(&e.ZaposlenikID, &e.Name, &e.Surname, &e.Plate)
	switch {
	case err == sql.ErrNoRows:
		return nil, sql.ErrNoRows
	case err != nil:
		return nil, errors.New("Error while getting evidencija (" + err.Error() + ")")
	}
	return e, nil
}

func getEvidencije(db *db.Database, query string) (*Evidencije, error) {
	eList := &Evidencije{}

	rows, err := db.Conn.Query(query)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	hasRow := rows.Next()

	if !hasRow {
		return nil, sql.ErrNoRows
	}

	for hasRow {
		var e Evidencija
		err = rows.Scan(&e.ZaposlenikID, &e.Name, &e.Surname, &e.Plate)
		if err != nil {
			return nil, errors.New("Error while getting evidencija (" + err.Error() + ")")
		}
		eList.EvidencijaList = append(eList.EvidencijaList, e)
		hasRow = rows.Next()
	}

	return eList, nil
}

// func getEvidencijeWithParam(db *db.Database, query string, param string) (*Evidencije, error) {
// 	eList := &Evidencije{}

// 	rows, err := db.Conn.Query(query, param)
// 	if err != nil {
// 		return nil, err
// 	}

// 	defer rows.Close()

// 	hasRow := rows.Next()

// 	if !hasRow {
// 		return nil, sql.ErrNoRows
// 	}

// 	for hasRow {
// 		var e Evidencija
// 		err = rows.Scan(&e.ID, &e.ZaposlenikID, &e.Name, &e.Surname, &e.Plate)
// 		if err != nil {
// 			return nil, errors.New("Error while getting sensors (" + err.Error() + ")")
// 		}
// 		eList.EvidencijaList = append(eList.EvidencijaList, e)
// 		hasRow = rows.Next()
// 	}

// 	return eList, nil
// }
