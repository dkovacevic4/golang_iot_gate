package main

import (
	"log"

	"./data"
	"./db"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {

	db, err := db.Connect()

	if err != nil {
		log.Fatal(err)
	}

	dc := data.NewController(db)

	r := echo.New()
	r.Use(middleware.CORS())
	r.Use(middleware.Logger())
	r.Use(middleware.Recover())

	v1 := r.Group("/v1")
	{
		v1.GET("/evidencije", dc.GetEvidencije)
		v1.GET("/evidencije/:evidencijaTable", dc.GetEvidencija)
	}

	r.Start("127.0.0.1:8000")

}
