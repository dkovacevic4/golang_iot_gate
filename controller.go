package data

import (
	"database/sql"

	"../db"
	"github.com/labstack/echo"
)

// Controller for database
type Controller struct {
	db *db.Database
}

// ErrorResponse for response
type ErrorResponse struct {
	Message     string `json:"message"`
	Description string `json:"description"`
}

// NewController create new data output controller
func NewController(db *db.Database) *Controller {
	return &Controller{db: db}
}

// GetEvidencije get information about all evidencije
func (dc *Controller) GetEvidencije(c echo.Context) error {
	return dc.getEvidencije(c)
}

// GetEvidencija get information about specific sensor
func (dc *Controller) GetEvidencija(c echo.Context) error {
	return dc.getEvidencija(c)
}

func (dc *Controller) getEvidencija(c echo.Context) error {
	strTable := c.Param("evidencijaTable")
	s, err := getEvidencijaByTable(dc.db, strTable)

	switch {
	case s != nil:
		return c.JSON(200, s)
	case err == sql.ErrNoRows:
		return c.JSON(404, ErrorResponse{"Evidencija not found", "Evidencija with specified ID was not found"})
	case err != nil:
		return c.JSON(500, ErrorResponse{"Error while getting evidencija", "There was an server error while getting evidencije"})
	default:
		return c.JSON(404, ErrorResponse{"Evidencija not found", "Evidencija with specified ID was not found"})
	}
}

func (dc *Controller) getEvidencije(c echo.Context) error {

	s, err := getAllEvidencije(dc.db)

	switch {
	case s != nil:
		return c.JSON(200, s)
	case err != nil:
		return c.JSON(500, ErrorResponse{"Error while getting evidencije", "There was an server error while getting evidencije"})
	default:
		return c.JSON(404, ErrorResponse{"Evidencija not found", "There are no evidencija in database"})
	}
}
