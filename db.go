package db

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

// Database handles DB connections
type Database struct {
	Conn *sql.DB
}

// Connect creates new Database struct and connects to it
func Connect() (*Database, error) {

	conn, err := sql.Open("mysql", "root:@tcp(127.0.0.1:3306)/vub_iot_gate")
	if err != nil {
		log.Fatal(err)
	}

	err = conn.Ping()
	if err != nil {
		log.Fatal(err)
	}

	return &Database{Conn: conn}, nil
}
